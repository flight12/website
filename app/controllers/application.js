// app/controllers/application.js
import Ember from 'ember';

export default Ember.Controller.extend({
  cableService: Ember.inject.service('cable'),

  setupConsumer: Ember.on('init', function() {
    var consumer = this.get('cableService').createConsumer('ws://localhost:3000/websocket');

    consumer.subscriptions.create("FlightOperationChannel", {
      connected() {
        Ember.debug("FlightOperationChannel#connected");
//        this.perform('hello', { foo: 'bar' });
      },
      received(data) {
        Ember.debug( "received(data) -> " + Ember.inspect(data) );
        this.get("store").pushPayload(data);

      },
      disconnected() {
        Ember.debug("FlightOperationChannel#disconnected");
      }
    });

  })
});
