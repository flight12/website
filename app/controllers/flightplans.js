import Ember from 'ember';

export default Ember.Controller.extend({
  columns: [
  {
    "propertyName": "id",
    "title": "ID"
  },
  {
    "propertyName": "departureAerodrome",
    "title": "From"
  },
  {
    "propertyName": "destinationAerodrome",
    "title": "To"
  },
  {
    "propertyName": "dateOfFlight",
    "title": "Date"
  },
  {
    "propertyName": "flightStatus",
    "title": "Status"
  }
]
});
