import DS from 'ember-data';

export default DS.Model.extend({
  departureAerodrome: DS.attr('string'),
  cruisingSpeed: DS.attr('number'),
  destinationAerodrome: DS.attr('string'),
  dateOfFlight: DS.attr('date'),
  aircraftIdentification: DS.attr('string'),
  totalEet: DS.attr('number'),
  personsOnBoard: DS.attr('number'),
  flightStatus: DS.attr('string')
});
