import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('flightplan-shortinfo', 'Integration | Component | flightplan shortinfo', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{flightplan-shortinfo}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#flightplan-shortinfo}}
      template block text
    {{/flightplan-shortinfo}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
